#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "tubtrans.h"

struct system sys;

/* function declarations */

static int cnt_to_int(xmlNode *a_node)
{
    int content;
    xmlChar *xmlcontent = xmlNodeGetContent(a_node);
    content = atoi((char *)xmlcontent);
    xmlFree(xmlcontent);
    return content;
}

static float cnt_to_float(xmlNode *a_node)
{
    float content;
    xmlChar *xmlcontent = xmlNodeGetContent(a_node);
    content = atof((char *)xmlcontent);
    xmlFree(xmlcontent);
    return content;
}

static xmlNode *find_tag(xmlNode *a_node, char *tag)
{
    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) tag))
                    return cur_node;
        }
    }

    return NULL;
}


static void get_pipe(xmlNode *a_node)
{
    /* We'll look for the parameters of the pipe */

    sys.pipe[sys.npipes] = malloc(sizeof(struct pipe));
    struct pipe *cur_pipe = sys.pipe[sys.npipes];
    sys.npipes += 1;

    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "fr"))
                    cur_pipe->fr = cnt_to_int(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "to"))
                    cur_pipe->to = cnt_to_int(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "fric"))
                    cur_pipe->fric = cnt_to_float(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "diam"))
                    cur_pipe->diam = cnt_to_float(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "a"))
                    cur_pipe->a = cnt_to_float(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "len"))
                    cur_pipe->len = cnt_to_float(cur_node);
        }

    }

}


static void get_valve(xmlNode *a_node)
{

    sys.valve[sys.nvalves] = malloc(sizeof(struct valve));
    struct valve *cur_valve = sys.valve[sys.nvalves];
    sys.nvalves += 1;

    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "node"))
                    cur_valve->node = cnt_to_int(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "lossc"))
                    cur_valve->lossc = cnt_to_float(cur_node);
        }

    }

}

static void get_reservoir(xmlNode *a_node)
{

    sys.reservoir[sys.nreservoirs] = malloc(sizeof(struct reservoir));
    struct reservoir *cur_reservoir = sys.reservoir[sys.nreservoirs];
    sys.nreservoirs += 1;

    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "node"))
                    cur_reservoir->node = cnt_to_int(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "head"))
                    cur_reservoir->head = cnt_to_float(cur_node);
        }

    }

}

static void get_junction(xmlNode *a_node)
{

    sys.junction[sys.njunctions] = malloc(sizeof(struct junction));
    struct junction *cur_junction = sys.junction[sys.njunctions];
    sys.njunctions += 1;

    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "node"))
                    cur_junction->node = cnt_to_int(cur_node);
        }

    }

}


static void transverse(xmlNode *a_node)
{
    xmlNode *cur_node = NULL;

    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "nnode"))
                sys.nnodes = cnt_to_int(cur_node);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "pipe"))
                get_pipe(cur_node->children);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "valve"))
                get_valve(cur_node->children);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "reservoir"))
                get_reservoir(cur_node->children);
            if(!xmlStrcmp(cur_node->name, (const xmlChar *) "junction"))
                get_junction(cur_node->children);
        }

        //transverse(cur_node->children);
    }
}




void parse_xml_file(const char *filename)
{
    xmlDoc *doc;
    xmlNodePtr cur;

    /* Init system structure */
    sys.nnodes = 0;
    sys.npipes = 0;
    sys.nreservoirs = 0;
    sys.nvalves = 0;
    sys.njunctions = 0;

    doc = xmlReadFile(filename, NULL, 0);
    if(!doc){
        fprintf(stderr, "Failed to parse '%s'.\n", filename);
        return;
    }

    cur = xmlDocGetRootElement(doc);

    if (cur == NULL) {
        fprintf(stderr,"empty document\n");
        xmlFreeDoc(doc);
        return;
    }

    if (xmlStrcmp(cur->name, (const xmlChar *) "case"))
        fprintf(stderr, "invalid document. Root node must be 'case'");
    else
        transverse(cur->children);

    xmlFreeDoc(doc);
    xmlCleanupParser();

}

void parse_xml_init(void)
{
    LIBXML_TEST_VERSION
}



