function J = jeval_char(x, xold, hlen, htim, param, valve)

    nvars = size(x,1);
    nsec = nvars / 2;

    Qold = xold(1:nsec);
    Hold = xold(nsec + 1:end);
    

    Q = x(1:nsec);
    H = x(nsec + 1:end);
    
    
    J = zeros(nvars, nvars);

    % interior points

    for i = (1:nsec)
        
        if ((i ~= 1) && (i ~= nsec))
        
            J(2*(i - 1) + 1, i) = 1;
            J(2*(i - 1) + 1, nsec + i) = (param.g*param.A)/param.ac;
            
            J(2*(i - 1) + 2, i) = 1;
            J(2*(i - 1) + 2, nsec + i) = -(param.g*param.A)/param.ac;
        
        elseif (i == 1) %upstream
            
            J(2*(i - 1) + 1, nsec + i) = -1;
            
            J(2*(i - 1) + 2, i) = 1;
            J(2*(i - 1) + 2, nsec + i) = -(param.g*param.A)/param.ac;
            
            
        else %endpipe

            J(2*(i - 1) + 1, i) = 1;
            J(2*(i - 1) + 1, nsec + i) = (param.g*param.A)/param.ac;
            
            % valve conditions
            Ca = (param.g*param.A)/(param.ac);
            Cv = (valve.tau*valve.Q0)^2/(Ca*valve.H0);
            
            J(2*(i - 1) + 2, i) = 2*Q(i) + Cv;
            
            if (abs(Q(i)) < 1e-4)
                J(2*(i - 1) + 2, i) = 1;
            end
            
        end

    end
    

end