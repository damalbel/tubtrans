clear all

%% Parameters PDE
g = 9.806;
D = 0.5;
f = 0.018;
A = pi*(D/2)^2;
R = f/(2*D*A);
a = 1200;
L = 600; % Length in m
T = 3; % Time in seconds
QL = 0.477; % Initial flow input
H0 = 150; % Initial pressure output
% g = 1;
% D = 1;
% f = 1;
% A = 1;
% R = 1;
% a = 1;
% L = 1; % Length in ... ?
% T = 1; % Time in seconds
% Q0 = 1; % Initial flow input
% HL = 2; % Initial pressure output

%% Parameters Kernel
shift = 0;
d = 2;
n = 4;
M = (2^n+1)^d;
N = (2^n+1)^d;
nepts = 40^d;
epsilon = sqrt(1/L);
gamma = sqrt(1/T);
% dsites = mod(CreatePoints(M,d,'u')+shift,1+eps).*repmat([L T],M,1);
% ctrs = mod(CreatePoints(N,d,'u')+shift,1+eps).*repmat([L T],N,1); % ctrs = dsites;
% epts = mod(CreatePoints(nepts,d,'u')+shift,1+eps).*repmat([L T],nepts,1);

% Interior points
dsites = CreateInteriorPoints2Du(M).*repmat([L T],M,1);
ctrs = CreateInteriorPoints2Du(N).*repmat([L T],N,1);
epts = CreatePoints(nepts,d,'u').*repmat([L T],nepts,1);
% Boundary points
initcond = [L*((0:M^(1/2)+1)/(M^(1/2)+1))' , zeros(M^(1/2)+2,1)]; % Points for the initial condition
inbound = [zeros(M^(1/2)+2,1) , T*((0:M^(1/2)+1)/(M^(1/2)+1))']; % Points for the boundary condition x=0
outbound = [L*ones(M^(1/2)+2,1) , T*((0:M^(1/2)+1)/(M^(1/2)+1))']; % Points for the boundary condition x=L
% inbound and outbound muts be the same size
% Initial conditions
Q_0 = @(x) QL+0*x(:,1); % Solution for steady state
H_0 = @(x) H0-R*QL*abs(QL)/(g*A)*x(:,1); % Solution for steady state
Qinit = Q_0(initcond);
Hinit = H_0(initcond);
ctrs = [ctrs;initcond];
ncond = size(initcond,1);
% Boundary conditions
Qt = @(x) QL+0*x(:,1); % Input flow
Ht = @(x) H0+0*x(:,1); % Output pressure
Qibound = Qt(outbound);
Hobound = Ht(inbound);
ctrs = [ctrs;inbound];
ncond = ncond+size(inbound,1);

%% anisotropic IMQ kernel,Li & Mao [LM11] . For Q we have 1:N and for H N+1:2N
% For the interior points
Kernel = @(x,z) (1+epsilon^2*(repmat(x(:,1),1,size(z,1))-repmat(z(:,1)',size(x,1),1)).^2+gamma^2*(repmat(x(:,2),1,size(z,1))-repmat(z(:,2)',size(x,1),1)).^2).^(-1/2);
Kerneldx = @(x,z) -epsilon^2*(repmat(x(:,1),1,size(z,1))-repmat(z(:,1)',size(x,1),1)).*(1+epsilon^2*(repmat(x(:,1),1,size(z,1))-repmat(z(:,1)',size(x,1),1)).^2+gamma^2*(repmat(x(:,2),1,size(z,1))-repmat(z(:,2)',size(x,1),1)).^2).^(-3/2);
Kerneldt = @(x,z) -gamma^2*(repmat(x(:,2),1,size(z,1))-repmat(z(:,2)',size(x,1),1)).*(1+epsilon^2*(repmat(x(:,1),1,size(z,1))-repmat(z(:,1)',size(x,1),1)).^2+gamma^2*(repmat(x(:,2),1,size(z,1))-repmat(z(:,2)',size(x,1),1)).^2).^(-3/2);
K = Kernel(dsites,ctrs);
Kdx = Kerneldx(dsites,ctrs);
Kdt = Kerneldt(dsites,ctrs);

D = @(x) [a^2*Kdx  g*A*Kdt  ;...
    (Kdt+R*abs(repmat(K*[x(1:N);x(2*N+1:2*N+ncond)],1,N+ncond)).*K)  g*A*Kdx  ;...
    [Kernel(initcond,ctrs) zeros(size(initcond,1),N+ncond)  ;  zeros(size(initcond,1),N+ncond) Kernel(initcond,ctrs)]  ;...
    [Kernel(inbound,ctrs) zeros(size(inbound,1),N+ncond)  ;  zeros(size(outbound,1),N+ncond) Kernel(outbound,ctrs)]];
J = @(x) [a^2*Kdx  g*A*Kdt  ;...
    (Kdt+2*repmat(K*[x(1:N);x(2*N+1:2*N+ncond)],1,N+ncond).*K.*sign(repmat(K*[x(1:N);x(2*N+1:2*N+ncond)],1,N+ncond)))  g*A*Kdx  ;...
    [Kernel(initcond,ctrs) zeros(size(initcond,1),N+ncond)  ;  zeros(size(initcond,1),N+ncond) Kernel(initcond,ctrs)]  ;...
    [Kernel(inbound,ctrs) zeros(size(inbound,1),N+ncond)  ;  zeros(size(outbound,1),N+ncond) Kernel(outbound,ctrs)]];

%% Newton Raphson for finding the coefficients
x0 = ones(2*(N+ncond),1);
xn = x0;
maxiter = 1; n = 0;
tol = 10^-7;
while norm((D(xn)*xn-[zeros(2*N,1);Qinit;Hinit;Qibound;Hobound]))>tol && n<maxiter
    n = n+1;
    dxn = J(xn)\(-(D(xn)*xn-[zeros(2*N,1);Qinit;Hinit;Qibound;Hobound]));
    xn = dxn+xn;
end
if n == maxiter
    fprintf('DID NOT CONVERGENCE.\n')
end
fprintf('Error is %d with %d iterations given %d max iterations.\n', norm((D(xn)*xn-[zeros(2*N,1);Qinit;Hinit;Qibound;Hobound])), n, maxiter)

%% Solutions for Q and H
% Q = @(x) Kernel(x,ctrs)*[xn(1:N);xn(2*N+1:2*N+ncond)];
% H = @(x) Kernel(x,ctrs)*[xn(N+1:2*N);xn(2*N+ncond+1:2*(N+ncond))];
Q = @(x) Kernel(x,ctrs)*xn(1:N+ncond);
H = @(x) Kernel(x,ctrs)*xn(N+ncond+1:end);

Qp = reshape(Q(epts),[sqrt(nepts),sqrt(nepts)]);
Hp = reshape(H(epts),[sqrt(nepts),sqrt(nepts)]);

plotsurf(Qp,1:sqrt(nepts),1:sqrt(nepts),'x','t','Q(x,t)')
figure
plotsurf(Hp,1:sqrt(nepts),1:sqrt(nepts),'x','t','H(x,t)')