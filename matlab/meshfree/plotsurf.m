function [] = plotsurf(K,dsites,ctrs,xl,yl,zl)
a=-25+90;
b=35;
surf(dsites,ctrs,K,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','none')
colormap autumn
axis tight
set(gca,'Fontsize',18)
xlabel(xl)
ylabel(yl)
zlabel(zl)
view(a,b)
camlight('headlight') 
lighting phong;
material metal;
end

