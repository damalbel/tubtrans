function [ points ] = CreateInteriorPoints2Du(N)
% Dimension 2 and uniform N points (N must be a power of 2)
m = N^(1/2)+2;
points = zeros(1,2);
vec = (1:m-1)/(m-1);
for i = 1:m-2
    for j = 1:m-2
        points = [points;[vec(i) vec(j)]];
    end
end
points = points(2:end,:);
end

