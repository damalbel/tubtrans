function J = jeval_init(x, nlen, hlen, param)

    nvars = size(x,1);
    nflow = nvars / 2;

    J = zeros(nvars, nvars);


    % aux loss head term
    
    aux = hlen*param.R/(param.g*param.A);
    
    for i = 1:nlen
        
        % Continuity
        J(i,i) = 1;
        J(i,i + 1) = -1;
        
        % Head loss
        
        J(nflow - 1 + i, nflow + i) = 1;
        J(nflow - 1 + i, nflow + i + 1) = -1;
        J(nflow - 1 + i, i + 1) = -2*aux*x(i + 1);

    end
    
    % boundary conditions
    
    J(end - 1, nflow + 1) = -1;
    J(end, nflow) = -2*x(nflow);
    J(end, end)   = (param.CdAg0^2)*2*param.g;


end