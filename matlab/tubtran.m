
%problem parameters

clear
clc

param = struct();
param.L = 600;
param.ac = 1200;
param.D = 0.5;
param.f = 0.018;
param.Hr = 150;
param.g = 9.806;
param.A = pi*(param.D/2)^2;
param.CdAg0 = 0.009;
param.R = param.f/(2*param.D*param.A);

maxiter = 40;
convtol = 10e-8;

%preliminary calculations

nlen = 5;
hlen = param.L / nlen;
nvar = (nlen + 1)*2;
htim = 0.1;
endtime = 10;

xk = ones(nvar,1);
%xk = [0.477 0.477 0.477 0.477 0.477 0.477, 150, 148.7, 147.4, 146.09, 144.79, 143.49]';
% Initialization

fprintf( 'Fluid transient: initialization procedure\n');


for i = 1:maxiter

    F = feval_init(xk, nlen, hlen, param);
    fprintf(' iteration %d, norm: %f\n', i, norm(F));
    
    if (norm(F) < convtol)
        fprintf( 'Newton-Rhapson converged in %g iterations\n', i );
        break;
    end
    
    J = jeval_init(xk, nlen, hlen, param);
    xdelta = -1*(J\F);
    xk = xk + xdelta;


    
end

if (norm(F) > convtol)
    fprintf( 'Newton-Rhapson did not converge\n')
end


% Some datastructures for the valve

valve = struct();
valve.tau = 0.929;
valve.Q0 = xk(nvar / 2);
valve.H0 = xk(end);

% Time stepping

fprintf( 'Integrating differential equations: ...\n\n');

solution = [xk];

for i = 1:(endtime/htim)
    
    
    if htim*i < 2.1
        valve.tau = 0;    
    else
        valve.tau = 0;
    end
    

    fprintf( 'TS: %f seconds. Valve aperture: %f\n', htim*i, valve.tau);
    [xk, error] = nes_ts(xk, convtol, maxiter, htim, hlen, param, valve);
    solution = [solution, xk];
    
end


figure

time = [0:htim:endtime];

subplot(2,2,1)

title('Head at valve');
plot(time, solution(12,:));

subplot(2,2,2);
title('Flow at valve');
plot(time, solution(6,:));

subplot(2,2,3);
title('Head at reservoir');
plot(time, solution(7,:));

subplot(2,2,4);
title('Flow at reservoir');
plot(time, solution(1,:));


figure
grid on
cc=hsv(6);

subplot(2,1,1);
hold on;
for i=1:6
    plot(time,solution(i,:),'color',cc(i,:));
end


subplot(2,1,2);
hold on;
for i=1:6
    plot(time,solution(i + 6,:),'color',cc(i,:));
end



figure

surf(solution(7:end,:))
