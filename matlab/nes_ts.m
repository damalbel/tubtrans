function [xk, error] = nes_ts(x0, tol, maxiter, htim, hlen, param, valve)

    xk = x0;
    xold = x0;
    error = 1;

    for i = 1:maxiter

        F = feval_char(xk, xold, hlen, htim, param, valve);
        %fprintf( ' Iteration %d, norm: %f\n', i , norm(F));
        if (norm(F) < tol)
            fprintf( '  Newton-Rhapson converged in %d iterations. Norm: %g\n', i, norm(F) );
            error = 0;
            break;
        end

        J = jeval_char(xk, xold, hlen, htim, param, valve);
        xdelta = -1*(J\F);
        xk = xk + xdelta;

    end

    if (norm(F) > tol)
        fprintf( '  Newton-Rhapson did not converge\n')
        error = 1;
    end



end