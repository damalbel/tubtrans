function F = feval_init(x, nlen, hlen, param)

    nvars = size(x,1);
    nflow = nvars / 2;

    F = zeros(nvars, 1);


    % aux loss head term
    
    aux = hlen*param.R/(param.g*param.A);
    for i = 1:nlen
        
        F(i) = x(i) - x(i + 1);
        F(nflow - 1 + i) = x(nflow + i) - x(nflow + i + 1) - ...
            aux*x(i+1)*abs(x(i + 1));

    end
    
    % boundary conditions
    
    F(end - 1) = param.Hr - x(nflow + 1); %upstream reservoir
    F(end)     = (param.CdAg0^2)*2*param.g*x(end) - x(nflow)*x(nflow); %valve


end