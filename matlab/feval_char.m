function F = feval_char(x, xold, hlen, htim, param, valve)

    nvars = size(x,1);
    nsec = nvars / 2;

    Qold = xold(1:nsec);
    Hold = xold(nsec + 1:end);
    

    Q = x(1:nsec);
    H = x(nsec + 1:end);
    
    
    F = zeros(nvars, 1);

    % interior points

    for i = (1:nsec)
        
        if ((i ~= 1) && (i ~= nsec))
        
            F(2*(i - 1) + 1) = Q(i) - Qold(i - 1) + (param.g*param.A)/param.ac*(H(i) - Hold(i - 1)) + ...
                param.R*htim*Qold(i - 1)*abs(Qold(i - 1));
            
            F(2*(i - 1) + 2) = Q(i) - Qold(i + 1) - (param.g*param.A)/param.ac*(H(i) - Hold(i + 1)) + ...
                param.R*htim*Qold(i + 1)*abs(Qold(i + 1));
        
        elseif (i == 1) %upstream
            
            F(2*(i - 1) + 1) = param.Hr - H(i);
            
            F(2*(i - 1) + 2) = Q(i) - Qold(i + 1) - (param.g*param.A)/param.ac*(H(i) - Hold(i + 1)) + ...
                param.R*htim*Qold(i + 1)*abs(Qold(i + 1));
            
            
        else %endpipe

            F(2*(i - 1) + 1) = Q(i) - Qold(i - 1) + (param.g*param.A)/param.ac*(H(i) - Hold(i - 1)) + ...
                param.R*htim*Qold(i - 1)*abs(Qold(i - 1));
            
            % valve pre-calcs
            Ca = (param.g*param.A)/(param.ac);
            Cv = (valve.tau*valve.Q0)^2/(Ca*valve.H0);
            Cp = Qold(i - 1) + Ca*Hold(i - 1) - param.R*htim*Qold(i - 1)*abs(Qold(i - 1));
            F(2*(i - 1) + 2) = Q(i)^2 + Cv*Q(i) - Cp*Cv;
            if (abs(Q(i)) < 1e-4)
                F(2*(i - 1) + 2) = Q(i);
            end
            
        end

    end
    

end