#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "tubtrans.h"


/* ===============================================================  */
/* This module contains routines for system data structure handling */
/* ===============================================================  */




void add_bound_up(int node, int pointer)
{

	if(sys.bound_up[node].node[0] == -1){
		sys.bound_up[node].node[0] = pointer;
	}else {
		sys.bound_up[node].node[1] = pointer;
	}

}

void add_bound_down(int node, int pointer)
{

    if(sys.bound_down[node].node[0] == -1){
        sys.bound_down[node].node[0] = pointer;
    }else {
        sys.bound_down[node].node[1] = pointer;
    }

}


void init_bound_structure()
{

	int i;
	for(i = 0; i < sys.nnodes; i++){
		sys.bound_up[i].node[0] = -1;
		sys.bound_up[i].node[1] = -1;
        sys.bound_down[i].node[0] = -1;
        sys.bound_down[i].node[1] = -1;
	}


}


/* frees system structure. To call after ending program */

void free_system_structure(void)
{
    int i;

    /* free allocated space for branches */

    for(i = 0; sys.pipe[i]; i++){
        free(sys.pipe[i]);
    }

    for(i = 0; sys.valve[i]; i++){
        free(sys.valve[i]);
    }

    for(i = 0; sys.reservoir[i]; i++){
        free(sys.reservoir[i]);
    }


}


void print_metadata(int ntimesteps, double htime)
{

    int i;

    /* Print some data in a txtfile for data processing purposes */

    FILE *f = fopen("sys.txt", "w");
    if (f == NULL) {
        printf("Error opening file!\n");
        exit(1);
    }

    fprintf(f, "ntimesteps %d htime %f\n", ntimesteps, htime);

    for(i = 0; sys.pipe[i]; i++){
        fprintf(f, "pipe %d pointer %d nlen %d\n", i, sys.pipe[i]->point,
            sys.pipe[i]->nlen);
    }

}

