import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

class pipe(object):

	def __init__(self, pointer, nlen, n):

		self.n = n
		self.pointer = pointer
		self.nlen = nlen


class system(object):

	" defines the structure of the pipe system"

	def __init__(self):

		self.pipes = {}

		filename = 'sys.txt'
		data = open(filename, 'r').readlines()

		for line in data:

			if 'ntimesteps' in line:
				line = " ".join(line.split()).split()
				self.ntimesteps = int(line[1])
				self.htime = float(line[3])

			elif 'pipe' in line:
				line = " ".join(line.split()).split()
				self.pipes[int(line[1])] = pipe(int(line[3]), int(line[5]), int(line[1]))


def readBinary(filename):

	'''
	name: readBinary
	input: file address

	description: this function reads a dense petsc
	matrix
	'''

	scalartype = np.dtype('>f8')
	inttype = np.dtype('>i4')


	fid = open(filename, 'rb')
	header = np.fromfile(fid, dtype=inttype, count=1)[0]
	M,N,nz = np.fromfile(fid, dtype=inttype, count=3)
	nz = M*N

	mat = np.fromfile(fid, dtype=scalartype, count=nz)
	mat = mat.reshape((M,N))

	return mat

def plot_pipe(sys, pipe, mat):

	time = np.arange(0, sys.htime * sys.ntimesteps +  sys.htime, sys.htime)

	data = mat[pipe.pointer:pipe.pointer + 2*(pipe.nlen + 1)]
	

	Q = data[::2]
	H = data[1::2]

	plt.subplot(2, 1, 1)
	plt.plot(time, Q[0], 'yo-')
	plt.title('Pipe #' + str(pipe.n) + ' : Upstreams node')
	plt.ylabel('Flow')
	plt.ticklabel_format(useOffset=False, axis='y')

	plt.subplot(2, 1, 2)
	plt.plot(time, H[0], 'r.-')
	plt.xlabel('time (s)')
	plt.ylabel('Head')
	plt.ticklabel_format(useOffset=False, axis='y')

	plt.savefig('output/upstream.png')
	plt.clf()

	plt.subplot(2, 1, 1)
	plt.plot(time, Q[-1], 'yo-')
	plt.title('Pipe #' + str(pipe.n) + ' : Downstreams node')
	plt.ylabel('Flow')
	plt.ticklabel_format(useOffset=False, axis='y')

	plt.subplot(2, 1, 2)
	plt.plot(time, H[-1], 'r.-')
	plt.xlabel('time (s)')
	plt.ylabel('Head')
	plt.ticklabel_format(useOffset=False, axis='y')

	plt.savefig('output/downstream.png')

	plt.clf()

	for i in range(pipe.nlen):
		plt.plot(time, Q[i])
	plt.ylabel('Flow')
	plt.title('Pipe #' + str(pipe.n) + ' : Flow profile')
	plt.ticklabel_format(useOffset=False, axis='y')
	plt.savefig('output/pipeFprofile.png')

	plt.clf()

	for i in range(pipe.nlen):
		plt.plot(time, H[i])
	plt.ylabel('Head')
	plt.title('Pipe #' + str(pipe.n) + ' : Head profile')
	plt.ticklabel_format(useOffset=False, axis='y')
	plt.savefig('output/pipeHprofile.png')

def plot_nodes(sys, mat):

	time = np.arange(0, sys.htime * sys.ntimesteps +  sys.htime, sys.htime)

	plt.clf()

	for i in sys.pipes:
		plt.plot(time, mat[sys.pipes[i].pointer] + 1)

	plt.xlabel('time (s)')
	plt.ylabel('Head')
	plt.ticklabel_format(useOffset=False, axis='y')

	plt.savefig('output/headnetwork.png')

	Q = mat[::2]
	H = mat[1::2]

	#plot contour
	plt.clf()

	fig = plt.figure()
	ax = fig.gca(projection='3d')

	n,m = Q.shape

	x = np.arange(m)
	y = np.arange(n)
	X, Y = np.meshgrid(x, y)
	surf = ax.plot_surface(X, Y, Q, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
	fig.colorbar(surf, shrink=0.5, aspect=5)

	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	plt.savefig('output/mflow.png')
	plt.clf()

	fig = plt.figure()
	ax = fig.gca(projection='3d')

	n,m = Q.shape

	x = np.arange(m)
	y = np.arange(n)
	X, Y = np.meshgrid(x, y)
	surf = ax.plot_surface(X, Y, H, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
	fig.colorbar(surf, shrink=0.5, aspect=5)

	ax.zaxis.set_major_locator(LinearLocator(10))
	ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

	plt.savefig('output/mhead.png')

if __name__ == '__main__':
	
	filename = "solution.out"
	mat = readBinary(filename)
	#print mat
	sys = system()
	plot_pipe(sys, sys.pipes[0], mat)
	plot_nodes(sys, mat)
