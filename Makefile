include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules

CC=gcc
CFLAGS=-Wall -g
LDFLAGS=-lm

OBJS=main.o parse.o feval_init.o feval_ts.o system.o

all: tubtrans

debug: CFLAGS += -DDEBUG
debug: all

tubtrans: $(OBJS)
	$(CLINKER) -o tubtrans $(OBJS) $(LDFLAGS) `xml2-config --libs` ${PETSC_LIB}

parse.o: parse.c tubtrans.h
	${PETSC_COMPILE} $(CFLAGS) -c `xml2-config --cflags` parse.c

feval_init.o: feval_init.c tubtrans.h
	${PETSC_COMPILE} $(CFLAGS) -c feval_init.c

feval_ts.o: feval_ts.c tubtrans.h
	${PETSC_COMPILE} $(CFLAGS) -c feval_ts.c

system.o: system.c tubtrans.h
	${PETSC_COMPILE} $(CFLAGS) -c system.c

main.o: main.c tubtrans.h
	${PETSC_COMPILE} $(CFLAGS) -c main.c

clean::
	$(RM) *.o
