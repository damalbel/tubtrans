#include "tubtrans.h"
#include <stdlib.h>

#define Q(I) xx[2*i]
#define H(I) xx[2*i + 1]

/* reservoid boundary */

static int valve(Vec x, Vec f, struct valve *valve)
{


	/* downstream boundary */
	int pointer = sys.bound_down[valve->node - 1].node[0];

	PetscScalar *xx;
	PetscScalar *ff;

	VecGetArray(x, &xx);
	VecGetArray(f, &ff);

	ff[pointer] = pow(valve->lossc, 2)*2.0*GRAV*xx[pointer] 
		- xx[pointer - 1]*xx[pointer - 1];

	VecRestoreArray(x, &xx);
	VecRestoreArray(f, &ff);


	return 0;

}



/* reservoid boundary */

static int reservoir(Vec x, Vec f, struct reservoir *reservoir)
{


	/* upstream boundary */
	int pointer = sys.bound_up[reservoir->node - 1].node[0];

	PetscScalar *xx;
	PetscScalar *ff;

	VecGetArray(x, &xx);
	VecGetArray(f, &ff);

	ff[pointer] = reservoir->head - xx[pointer + 1];

	VecRestoreArray(x, &xx);
	VecRestoreArray(f, &ff);


	return 0;

}

static int junction(Vec x, Vec f, struct junction *junction)
{

	int nbound_down = MAX_BOUND, nbound_up = MAX_BOUND, i;

	int *pointer_up = malloc(MAX_BOUND*sizeof(int));
	int *pointer_down = malloc(MAX_BOUND*sizeof(int));

	memcpy(pointer_up, &sys.bound_up[junction->node - 1].node[0],
		MAX_BOUND*sizeof(int)); 
	memcpy(pointer_down, &sys.bound_down[junction->node - 1].node[0],
		MAX_BOUND*sizeof(int)); 


	/* How many pipes are connected? */

	for(i = 0; i < MAX_BOUND; i++) {
		if(pointer_up[i] == -1) {
			nbound_up = i;
			break;
		}
	}

	for(i = 0; i < MAX_BOUND; i++) {
		if(pointer_down[i] == -1) {
			nbound_down = i;
			break;
		}
	}


	PetscScalar *xx;
	PetscScalar *ff;

	VecGetArray(x, &xx);
	VecGetArray(f, &ff);


	/* Flow continuity equation is evaluated in the first
	pipe connected downstremas to the junction */

	ff[pointer_down[0]] = 0;

	for(i = 0; i < nbound_down; i++){
		ff[pointer_down[0]] += xx[pointer_down[i] - 1];
	}

	for(i = 0; i < nbound_up; i++){
		ff[pointer_down[0]] -= xx[pointer_up[i]];
	}

	/* Head sameness is evaluated in the rest boundaries */

	for(i = 0; i < nbound_down - 1; i++){
		ff[pointer_down[i + 1]] = xx[pointer_down[0]] - xx[pointer_down[i + 1]] ;
	}

	for(i = 0; i < nbound_up; i++){
		ff[pointer_up[i]] = xx[pointer_down[0]] - xx[pointer_up[i] + 1] ;
	}

	// /* Same flow */
	// ff[pointer_up[0]] = xx[pointer_down[0] - 1] - xx[pointer_up[0]];

	// //  Same head 
	// ff[pointer_down[0]] = xx[pointer_up[0] + 1] - xx[pointer_down[0]];

	VecRestoreArray(x, &xx);
	VecRestoreArray(f, &ff);

	free(pointer_up);
	free(pointer_down);

	return 0;

}


/* interior point characteristic equations */

static int pipe(Vec x, Vec f, struct pipe *pipe)
{

	int i;
	int k = pipe->point;

	PetscScalar aux = (pipe->hlen*pipe->R)/(GRAV*pipe->area);



	PetscScalar *xx;
	PetscScalar *ff;
	PetscScalar *xp;

	VecGetArray(x, &xx);
	VecGetArray(f, &ff);

	xp = &xx[pipe->point];

	for(i = 0; i < pipe->nlen; i++){

		/* TODO: rewrite odd and even access to xx as
		Q and H with macros */

		ff[k + 1] = xp[2*i] - xp[2*(i + 1)];
		ff[k + 2] = xp[2*i + 1] - xp[2*(i + 1) + 1] -
			aux*xp[2*(i + 1)]*xp[2*(i + 1)];

		k += 2;
	}


	VecRestoreArray(x, &xx);
	VecRestoreArray(f, &ff);


	return 0;

}

/* function evaluation for petsc snes */

PetscErrorCode feval_init(SNES snes,Vec x,Vec f, void *ctx)
{

	int i;

	for(i = 0; sys.pipe[i]; i++)
		pipe(x, f, sys.pipe[i]);

	for(i = 0; sys.reservoir[i]; i++)
		reservoir(x, f, sys.reservoir[i]);

	for(i = 0; sys.valve[i]; i++)
		valve(x, f, sys.valve[i]);

	for(i = 0; sys.junction[i]; i++)
		junction(x, f, sys.junction[i]);		

	return 0;

}