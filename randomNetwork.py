#user input parameters
npip = 10
outputName = 'test5'

#pre-calculations
nnode = npip + 1


output = open(outputName +'.xml','w')


output.write('<?xml version="1.0"?>\n')
output.write('<case>\n')
output.write('	<nnode>%d</nnode>\n' % nnode)
output.write('\n')
# write boundary conditions

output.write('	<reservoir id="1">\n')
output.write('		<node>1</node>\n')
output.write('		<head>150</head>\n')
output.write('	</reservoir>\n')
output.write('\n')
output.write('	<valve id="1">\n')
output.write('		<node>%d</node>\n' % nnode)
output.write('		<lossc>0.009</lossc>\n')
output.write('	</valve>\n')
output.write('\n')
# write pipes and junctions

for i in range(npip):
	output.write('\n')
	output.write('	<pipe id="%d">\n' % (i + 1))
	output.write('		<fr>%d</fr>\n' % (i + 1))
	output.write('		<to>%d</to>\n' % (i + 2))
	output.write('    	<fric>0.018</fric>\n')
	output.write('    	<diam>0.5</diam>\n')
	output.write('    	<a>1200</a>\n')
	output.write('    	<len>600</len>\n')
	output.write('	</pipe>\n')

for i in range(nnode - 2):
	output.write('\n')
	output.write('	<junction id="%d">\n' % (i + 1))
	output.write('		<node>%d</node>\n' % (i + 2))
	output.write('	</junction>\n')

output.write('</case>\n')