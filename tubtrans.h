#ifndef NETWORK_H
#define NETWORK_H

#define MAX_BUS 50
#define MAX_BRANCH 100
#define MAX_BOUND 2


#define GRAV 9.806

#include <petscsnes.h>






/* for now we will use this structure to store bound position 
    but it won't scale with looped networks. Maybe a linked list
    will be useful?? Depends on parallelization */

struct bound_up {
    int node[MAX_BOUND];
};

struct bound_down {
    int node[MAX_BOUND];
};

struct reservoir {
	int node;
	float head;
};

struct valve {
	int node;
	float lossc;
};

struct junction {
    int node;
};


struct pipe {
    int fr;
    int to;
    float fric; /* friction coeff */
    float diam; /* diameter */
    float a;  /* speed */
    float len;
    float area;
    float R;

    /*discretization*/
    float hlen;
    int nlen;

    /*position*/
    int point;
};


struct system {

    int nvar; 
    int nnodes;
    int npipes;
    int nvalves;
    int nreservoirs;
    int njunctions;

    Vec xold;
    
    struct pipe *pipe[MAX_BRANCH];
    struct reservoir *reservoir[MAX_BUS];
    struct valve *valve[MAX_BUS];
    struct junction *junction[MAX_BUS];

    struct bound_up *bound_up;
    struct bound_down *bound_down;
    int *node_bound;
}; 



extern struct system sys;

void parse_xml_init(void);
void parse_xml_file(const char *filename);
void free_system_structure(void);
void print_metadata(int ntimesteps, double htime);

/* system module */
void init_bound_structure();
void add_bound(int node, int pointer);

/* function evaluation and jacobian */
PetscErrorCode feval_init(SNES snes, Vec x, Vec f, void*);
PetscErrorCode feval_ts(SNES snes, Vec x, Vec f, void*);


#endif
