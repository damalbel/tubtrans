static char help[] = "Analysis of hydraulic transients in pipe networks.\n\n";

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "tubtrans.h"

/* 
    tubtrans: analysis of hydraulic transients in pipe networks
*/



int main(int argc, char **argv)
{
    PetscViewer    viewer;
    SNES           snes;         /* nonlinear solver context */
    Vec            x,r;          /* solution, residual vectors */
    PetscInt       i, ierr, nTimeSteps;
    PetscBool      fileflg;
    PetscScalar    deltatim, endTime;
    PetscScalar    tstep = 0.0;
    PetscScalar    *ss;

    Mat SOL;

    const PetscScalar    *xx;

    char           file[PETSC_MAX_PATH_LEN];

    PetscInt size = 1; /*sequential for now*/

    parse_xml_init();
    PetscInitialize(&argc, &argv, "petscopt", help);
    MPI_Comm_size(PETSC_COMM_SELF,&size);


    SNESCreate(PETSC_COMM_SELF,&snes);


    /* read case data */


    PetscOptionsGetString(NULL, "-f", file, PETSC_MAX_PATH_LEN, &fileflg);
    
    if (!fileflg) {
    	printf("Loading testing case\n");
    	parse_xml_file("input/test.xml");	
    } else {
	   parse_xml_file(file);
    }

    printf("Loaded case file\n");


    /* Determine each pipe segment discretization based on delta time and Cn */

    ierr = PetscOptionsGetScalar(PETSC_NULL, "-deltatim", &deltatim, PETSC_NULL); CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(PETSC_NULL, "-endtime", &endTime, PETSC_NULL); CHKERRQ(ierr);

    nTimeSteps = round(endTime/deltatim);

    /* System structure initialization */
    
    sys.nvar = 0;
    sys.bound_up   = malloc(sys.nnodes * sizeof(struct bound_up));
    sys.bound_down = malloc(sys.nnodes * sizeof(struct bound_down));
    init_bound_structure();

    for (i = 0; sys.pipe[i]; i++){

        sys.pipe[i]->hlen = sys.pipe[i]->a * deltatim;
        sys.pipe[i]->nlen = round(sys.pipe[i]->len / sys.pipe[i]->hlen);
        sys.pipe[i]->hlen = sys.pipe[i]->len / sys.pipe[i]->nlen;

        sys.pipe[i]->area = PETSC_PI*pow(sys.pipe[i]->diam/2.0, 2);
        sys.pipe[i]->R = sys.pipe[i]->fric/(2.0 * sys.pipe[i]->diam * sys.pipe[i]->area);

        /* pipe pointers */
        sys.pipe[i]->point = sys.nvar;
        sys.nvar += (sys.pipe[i]->nlen + 1) * 2;
        
        /* network node boundaries */
        add_bound_up(sys.pipe[i]->fr - 1, sys.pipe[i]->point);
        add_bound_down(sys.pipe[i]->to - 1, sys.nvar - 1);

        #ifdef DEBUG
        printf("Pipe %i. nlen: %i. hlen: %f \n", i, sys.pipe[i]->nlen,
            sys.pipe[i]->hlen);
        #endif
    }


    /* Create petsc vectors and matrices*/

    ierr = VecCreate(PETSC_COMM_SELF, &x);CHKERRQ(ierr);
    ierr = VecSetSizes(x, PETSC_DECIDE, sys.nvar);CHKERRQ(ierr);
    ierr = VecSetFromOptions(x);CHKERRQ(ierr);
    ierr = VecDuplicate(x, &r);CHKERRQ(ierr);
    ierr = VecSet(x, 1.0);CHKERRQ(ierr);

    ierr = MatCreate(MPI_COMM_SELF,&SOL);CHKERRQ(ierr);
    ierr = MatSetSizes(SOL,PETSC_DECIDE,PETSC_DECIDE,sys.nvar,nTimeSteps + 1);CHKERRQ(ierr);
    ierr = MatSetType(SOL,MATSEQDENSE);CHKERRQ(ierr);
    ierr = MatSetUp(SOL);
    ierr = MatAssemblyBegin(SOL,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(SOL,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


    /* snes */
    ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
    ierr = SNESSetFunction(snes, r, feval_init, NULL);CHKERRQ(ierr);
    ierr = SNESSolve(snes, NULL, x);CHKERRQ(ierr);


    /* store initial vector */

    ierr = MatDenseGetArray(SOL, &ss);CHKERRQ(ierr);
    ierr = VecGetArrayRead(x, &xx);CHKERRQ(ierr);
    ierr = PetscMemcpy(ss, xx, sys.nvar*sizeof(PetscScalar));CHKERRQ(ierr);
    ierr = VecRestoreArrayRead(x, &xx);CHKERRQ(ierr);
    ierr = MatDenseRestoreArray(SOL, &ss);CHKERRQ(ierr);

    /* Some printing stuff */
    printf("SYSTEM SUMMARY\n\n");
    printf("Number of variables: %d\n", sys.nvar);
    printf("Number of nodes: %d\n", sys.nnodes);
    printf("Number of pipes: %d\n", sys.npipes);
    


    #ifdef DEBUG
        printf("Printing solution vector...\n");
        ierr = VecView(x, PETSC_VIEWER_STDOUT_SELF);
    #endif


    /* Time stepping loop */

    printf("Integrating characteristic equations ...\n");

    for(i = 0; i < nTimeSteps; i++){

        printf("TS: %f\n", deltatim*i);

        /* re-set snes */
        ierr = SNESSetFromOptions(snes);CHKERRQ(ierr);
        ierr = SNESSetFunction(snes, r, feval_ts, NULL);CHKERRQ(ierr);
        ierr = VecDuplicate(x, &sys.xold);CHKERRQ(ierr);
        ierr = VecCopy(x, sys.xold);CHKERRQ(ierr);
        ierr = SNESSolve(snes, NULL, x);CHKERRQ(ierr); 

        #ifdef DEBUG
            printf("Printing solution vector...\n");
            ierr = VecView(x, PETSC_VIEWER_STDOUT_SELF);
        #endif

        /* store initial vector */

        ierr = MatDenseGetArray(SOL, &ss);CHKERRQ(ierr);
        ierr = VecGetArrayRead(x, &xx);CHKERRQ(ierr);
        ierr = PetscMemcpy(ss + (i + 1)*sys.nvar, xx, sys.nvar*sizeof(PetscScalar));CHKERRQ(ierr);
        ierr = VecRestoreArrayRead(x, &xx);CHKERRQ(ierr);
        ierr = MatDenseRestoreArray(SOL, &ss);CHKERRQ(ierr);

        tstep = tstep + deltatim;

    }

    /* write solutions */

    ierr = PetscViewerBinaryOpen(PETSC_COMM_SELF,"solution.out",FILE_MODE_WRITE,&viewer);CHKERRQ(ierr);
    ierr = PetscViewerSetFormat(viewer,PETSC_VIEWER_NATIVE);CHKERRQ(ierr);
    ierr = MatView(SOL,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);


    print_metadata(nTimeSteps, deltatim);

    /* free structures */

    ierr = MatDestroy(&SOL);CHKERRQ(ierr);
    ierr = SNESDestroy(&snes);CHKERRQ(ierr);
    ierr = VecDestroy(&x);CHKERRQ(ierr);
    ierr = VecDestroy(&r);CHKERRQ(ierr);



    free_system_structure();

    PetscFinalize();



    return 0;

}
